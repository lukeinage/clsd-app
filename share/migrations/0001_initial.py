# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import share.models


class Migration(migrations.Migration):

    dependencies = [
        ('challenges', '0010_auto_20150206_2204'),
        ('app_user', '0004_auto_20150209_1418'),
    ]

    operations = [
        migrations.CreateModel(
            name='SharePost',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(null=True, upload_to=share.models.get_share_post_media_path, blank=True)),
                ('challenge', models.ForeignKey(to='challenges.ChallengeList')),
                ('user', models.ForeignKey(to='app_user.UserProfile')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
