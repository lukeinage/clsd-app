from django.contrib import admin
from share.models import SharePost

# registers models for editing in the site backend
admin.site.register(SharePost)