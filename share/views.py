from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.core.urlresolvers import reverse
from django.db import IntegrityError
from django.core.files.images import get_image_dimensions
from django import forms

from app_user.models import UserProfile
from share.models import SharePost
from challenges.models import Challenge, ChallengeList
from points import points_controller
from app_user.AppUserController import get_header_string

# Form for sharing posts, checks if uploaded image is too big
class TwitterImageForm(forms.Form):
	image = forms.ImageField(label='Optional - Share along with an image! (Limit 1MB)', required=False)

	def clean_image(self):
		picture = self.cleaned_data.get("image")
		if not picture:
			return None
		else:
			w, h = get_image_dimensions(picture)
			if w > 2048:
				raise forms.ValidationError("Width %i pixels wide. Please limit images to 1024x1024" % w)
			if h > 2048:
				raise forms.ValidationError("Height %i pixels wide. Please limit images to 1024x1024" % h)
			if picture._size > 1*1024*1024:
					raise forms.ValidationError("The image should be less than 1MB")
		return picture


# Share challenge main handler. let you create a post
def share_challenge(request):
	# Make sure the passed id hasn't been tampered with
	try:
		id = int(request.GET.get('challenge_id'))
	except ValueError:
		raise Http404("Malformed URL")

	# Get all user and challenge info and check it is correct
	profile = get_object_or_404(UserProfile, user=request.user)
	challenge = get_object_or_404(Challenge, id=id)
	user_challenge = get_object_or_404(ChallengeList, user=profile, challenge=challenge)

	if request.method == 'POST':
		form = TwitterImageForm(request.POST, request.FILES)
		if form.is_valid():
			try:
				if SharePost.objects.filter(challenge=user_challenge).exists() is False:
					points_controller.assign_points(profile, 1000, "share", user_challenge.id)

				t = SharePost()

				t.challenge = user_challenge
				t.user = profile
				t.save()

				# saving the twitter image
				if form.cleaned_data['image'] is not None:
					t.image = form.cleaned_data['image']

				t.save()

				return HttpResponseRedirect(reverse('share:share_post') + "?post_id=" + str(t.pk))
			except IntegrityError:
				pass
	else:
		form = TwitterImageForm()

	return render(request, 'share/share.html', {'title': ': Share', 'header_string': get_header_string(profile), 'user_challenge': user_challenge, 'form': form})


# returns the users post screen to post their story, if not logged a story summary is built
def share_post(request):
	# Make sure the passed id hasn't been tampered with
	try:
		id = int(request.GET.get('post_id'))
	except ValueError:
		raise Http404("Malformed URL")

	post = get_object_or_404(SharePost, id=id)
	user_challenge = post.challenge
	profile = post.user
	
	post_image = None
	if post.image:
		post_image = request.build_absolute_uri(post.image.url)

	logged_in = False
	header_string = 'Shared post'
	if request.user.id == profile.user.id:
		header_string = 'Sharing Post!'
		logged_in = True

	return render(request, 'share/share_post.html', {'title': ': Share', 'header_string': header_string, 'user_challenge': user_challenge, 'profile': profile, 'post_image': post_image, 'path': request.path, 'logged_in': logged_in})