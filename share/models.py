from django.db import models
from app_user.models import UserProfile
from challenges.models import ChallengeList
import os


def get_share_post_media_path(instance, filename):
	file_name, file_extension = os.path.splitext(filename)
	return os.path.join('share/post/' + str(instance.pk) + '/img' + file_extension)


# Stores different teams
class SharePost(models.Model):
	user = models.ForeignKey(UserProfile)
	image = models.ImageField(upload_to=get_share_post_media_path, null=True, blank=True)
	challenge = models.ForeignKey(ChallengeList)

	def __unicode__(self):
		return str(self.user) + str(self.challenge.challenge.id)