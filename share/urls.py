from django.conf.urls import patterns, url

from share import views

# routes to the correct view handler
urlpatterns = patterns('',
	url(r'^share_challenge/$', views.share_challenge, name='share_challenge'),
	url(r'^share_post/$', views.share_post, name='share_post'),
)