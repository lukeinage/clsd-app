var BrowserTools = function()
{
	this.init();
};

BrowserTools.instance = null;
BrowserTools.prototype._arrWindowListeners = null;

BrowserTools.getInstance = function()
{
	if(BrowserTools.instance == null)
	{
		BrowserTools.instance = new BrowserTools();
	}
	return BrowserTools.instance;
};

BrowserTools.prototype.init = function()
{
	this.onWindowResize = this.onWindowResize.bind(this);

	BrowserTools.prototype._arrWindowListeners = new Array();
	window.addEventListener("resize", this.onWindowResize, false);
};

BrowserTools.prototype.addWindowResizeListener = function(callback)
{
	this._arrWindowListeners.push(callback);
};

BrowserTools.prototype.onWindowResize = function()
{
	for(callback in this._arrWindowListeners)
	{
		this._arrWindowListeners[callback](window.innerWidth, window.innerHeight);
	}
};