/**
 * Manages Navigation bar operation
 * @constructor
 */
var NavbarController = function()
{
	this.init();
};

NavbarController.prototype.init = function()
{
	this.showingNavbar = true;
	this.mobileMenuHandler = this.mobileMenuHandler.bind(this);
	this.onWindowResize = this.onWindowResize.bind(this);
	BrowserTools.getInstance().addWindowResizeListener(this.onWindowResize);
	this.blMenuOpen = false;
	$(".navbar-item-menu-mobile").click(this.mobileMenuHandler);
};

NavbarController.prototype.blMenuOpen = null;
NavbarController.prototype.showingNavbar = null;

/**
* resize
* Changes to the mobile menu or desktop menu based on screen size
* updates the background sizes
*/
NavbarController.prototype.onWindowResize = function(width, height) {
	if(this.showingNavbar == false) { return; }
	//checks for mobile menu
	if (width < height) {
		$(".navbar-item").each(
			function (index) {
				this.style.display = "none";
			}
		);

		$(".navbar-item-menu-mobile").each(
			function (index) {
				this.style.display = "inline-block";
			}
		);

		$(".navbar-title").css({
			"float": "left"
		});
	}
	else
	{
		$(".navbar-item").each(
			function (index) {
				this.style.display = "inline-block";
			}
		);

		$(".navbar-item-menu-mobile").each(
			function (index) {
				this.style.display = "none";
			}
		);

		$(".navbar-item-mobile").each(
			function (index) {
				this.style.display = "none";
			}
		);

		$(".navbar-title").css({
			"float": "",
			"margin-left": "%"
		});
	}
};

/**
 * mobileMenuHandler
 * Opens and closes the menu
 */
NavbarController.prototype.mobileMenuHandler = function()
{
	if(this.blMenuOpen)
	{
		$(".navbar-item-mobile").each(
			function(index){
				this.style.display = "none";
			}
		);
		this.blMenuOpen = false;
	}
	else
	{
		$(".navbar-item-mobile").each(
			function(index){
				this.style.display = "block";
			}
		);
		this.blMenuOpen = true;
	}
};

NavbarController.prototype.setShowingNavbar = function(showingNavbar)
{
	this.showingNavbar = showingNavbar;
	$(".navbar-item").each(
		function (index) {
			this.style.display = "none";
		}
	);
	$(".navbar-item-menu-mobile").each(
		function (index) {
			this.style.display = "none";
		}
	);

	$(".navbar-item-mobile").each(
		function (index) {
			this.style.display = "none";
		}
	);

	$(".navbar-title").css({
		"float": "",
		"margin-left": "%"
	});
}