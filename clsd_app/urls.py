from django.conf.urls import patterns, include, url
from django.contrib import admin

import settings

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'clsd_app.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^', include('dashboard.urls', namespace="dashboard")),
    url(r'^dashboard/', include('dashboard.urls', namespace="dashboard")),
    url(r'^challenges/', include('challenges.urls', namespace="challenges")),
    url(r'^teams/', include('teams.urls', namespace="teams")),
    url(r'^app_user/', include('app_user.urls', namespace="app_user")),
     url(r'^share/', include('share.urls', namespace="share")),
    url(r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
        }))