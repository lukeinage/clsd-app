from django.db import models
from django.utils import timezone

from app_user.models import UserProfile


# Challenge Lookup Table
class Challenge(models.Model):
	challenge_title = models.CharField(max_length=64)
	challenge_description = models.TextField(max_length=1024)
	challenge_difficulty = models.IntegerField(default=5)
	challenge_time_in_days = models.IntegerField(default=7)
	challenge_ongoing = models.BooleanField(default=True)
	challenge_completion_user_failed = models.CharField(max_length=64)
	challenge_completion_user_tried = models.CharField(max_length=64)
	challenge_completion_user_did_good = models.CharField(max_length=64)
	challenge_completion_user_did_great = models.CharField(max_length=64)
	date_start = models.DateField()
	date_end = models.DateField()
	is_active = models.BooleanField(default=True)

	def __unicode__(self):
		return self.challenge_title


# User challenge list
class ChallengeList(models.Model):
	challenge = models.ForeignKey(Challenge)
	user = models.ForeignKey(UserProfile, null=True, blank=True)
	has_completed = models.BooleanField(default=False)
	level_of_complete = models.IntegerField(null=True, blank=True)
	start_date = models.DateTimeField('Challenge Start Date')
	achieved_date = models.DateTimeField(null=True, blank=True)

	def __unicode__(self):
		return self.user.user.username + " - " + self.challenge.challenge_title