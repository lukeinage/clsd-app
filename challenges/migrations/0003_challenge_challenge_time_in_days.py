# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('challenges', '0002_auto_20150205_1608'),
    ]

    operations = [
        migrations.AddField(
            model_name='challenge',
            name='challenge_time_in_days',
            field=models.IntegerField(default=7),
            preserve_default=False,
        ),
    ]
