# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('challenges', '0003_challenge_challenge_time_in_days'),
    ]

    operations = [
        migrations.AlterField(
            model_name='challenge',
            name='challenge_difficulty',
            field=models.IntegerField(default=5),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='challenge',
            name='challenge_points',
            field=models.IntegerField(default=5),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='challenge',
            name='challenge_time_in_days',
            field=models.IntegerField(default=7),
            preserve_default=True,
        ),
    ]
