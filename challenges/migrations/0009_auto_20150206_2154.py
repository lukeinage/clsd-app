# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('challenges', '0008_auto_20150206_1122'),
    ]

    operations = [
        migrations.AlterField(
            model_name='challengelist',
            name='achieved_date',
            field=models.DateTimeField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
