# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_user', '0003_auto_20150204_1413'),
    ]

    operations = [
        migrations.CreateModel(
            name='ChallengeList',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('has_completed', models.BooleanField(default=False)),
                ('target_date', models.DateTimeField(verbose_name=b'Challenge Target Date')),
                ('achieved_date', models.DateTimeField(verbose_name=b'Challenge Complete Date')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Challenges',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('challenge_title', models.CharField(max_length=64)),
                ('challenge_description', models.CharField(max_length=256)),
                ('challenge_difficulty', models.IntegerField()),
                ('challenge_points', models.IntegerField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='challengelist',
            name='challenge',
            field=models.ForeignKey(to='challenges.Challenges'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='challengelist',
            name='user',
            field=models.ForeignKey(to='app_user.UserProfile'),
            preserve_default=True,
        ),
    ]
