# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('challenges', '0007_auto_20150205_1631'),
    ]

    operations = [
        migrations.AlterField(
            model_name='challengelist',
            name='achieved_date',
            field=models.DateTimeField(null=True),
            preserve_default=True,
        ),
    ]
