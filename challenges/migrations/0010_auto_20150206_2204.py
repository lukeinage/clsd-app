# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('challenges', '0009_auto_20150206_2154'),
    ]

    operations = [
        migrations.AddField(
            model_name='challenge',
            name='challenge_completion_user_did_good',
            field=models.CharField(default='', max_length=64),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='challenge',
            name='challenge_completion_user_did_great',
            field=models.CharField(default='', max_length=64),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='challenge',
            name='challenge_completion_user_failed',
            field=models.CharField(default='', max_length=64),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='challenge',
            name='challenge_completion_user_tried',
            field=models.CharField(default='', max_length=64),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='challengelist',
            name='level_of_complete',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
