# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('challenges', '0005_challenge_challenge_ongoing'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='challenge',
            name='challenge_points',
        ),
    ]
