# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('challenges', '0004_auto_20150205_1616'),
    ]

    operations = [
        migrations.AddField(
            model_name='challenge',
            name='challenge_ongoing',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
    ]
