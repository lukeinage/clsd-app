# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('challenges', '0006_remove_challenge_challenge_points'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='challengelist',
            name='target_date',
        ),
        migrations.AddField(
            model_name='challengelist',
            name='start_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 5, 16, 31, 12, 618000, tzinfo=utc), verbose_name=b'Challenge Start Date'),
            preserve_default=False,
        ),
    ]
