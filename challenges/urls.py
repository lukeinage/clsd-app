from django.conf.urls import patterns, url

from challenges import views

# routes to the correct view handler
urlpatterns = patterns('',
	url(r'^$', views.challenge_index, name='index'),
	url(r'^challenge_update/$', views.challenge_update, name='challenge_update'),
	url(r'^challenge_complete/$', views.challenge_complete, name='challenge_complete'),
	url(r'^challenge_reward/$', views.challenge_reward, name='challenge_reward'),

)