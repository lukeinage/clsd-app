from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.core.urlresolvers import reverse
from django.utils import timezone
from django import forms
from dashboard.templatetags import dashboard_extras
import json

from app_user.models import UserProfile
from challenges.models import Challenge, ChallengeList
from points import points_controller
from app_user.AppUserController import get_header_string


# Chooser for the current challenge, uses an AJAX request
class ChallengeForm(forms.Form):
	challenge_id = forms.IntegerField()


# Survery after challenge has been completed
class ChallengeCompleteForm(forms.Form):
	challenge_status = forms.ChoiceField(widget=forms.RadioSelect(), required=True, label="")


# Main challenge page, shows different user challenge data
def challenge_index(request):
	profile = get_object_or_404(UserProfile, user=request.user)
	# get different lists
	user_challenge_list = ChallengeList.objects.filter(user=profile)
	completed_list = user_challenge_list.filter(user=profile, level_of_complete__isnull=False)
	challenge_list = Challenge.objects.exclude(id__in=[c.challenge.id for c in user_challenge_list])\
		.exclude(is_active=False)\
		.order_by('-challenge_difficulty')

	for unfiltered_challenged in challenge_list:
		if timezone.now().date() < unfiltered_challenged.date_start or timezone.now().date() > unfiltered_challenged.date_end:
			challenge_list = challenge_list.exclude(pk=unfiltered_challenged.pk)


	# remove ongoing challenges if they have been completed
	user_challenge_list = user_challenge_list.filter(level_of_complete__isnull=True)
	return render(request, 'challenges/index.html', {'title': ': Your Challenges', 'header_string': get_header_string(profile), 'user_challenge_list': user_challenge_list, 'challenge_list': challenge_list, 'completed_list': completed_list})


# for AJAX challenge update
def challenge_update(request):
	response_data = {
		'success': False
	}
	profile = UserProfile.objects.get(user=request.user)
	if profile:
		if request.method == 'POST':
			form = ChallengeForm(request.POST)
			if form.is_valid():
				challenge = Challenge.objects.get(pk=form.cleaned_data['challenge_id'])
				if challenge:
					cl = ChallengeList(challenge=challenge, user=profile, start_date=timezone.now())
					cl.save()
					response_data['success'] = True
	return HttpResponse(json.dumps(response_data), 'application/javascript')


# Final challenge complete page, sets challenges as complete
def challenge_complete(request):
	# Make sure the passed id hasn't been tampered with
	try:
		id = int(request.GET.get('challenge_id'))
	except ValueError:
		raise Http404("Malformed URL")

	# Get all user and challenge info and check it is correct
	profile = get_object_or_404(UserProfile, user=request.user)
	challenge = get_object_or_404(Challenge, id=id)
	user_challenge = get_object_or_404(ChallengeList, user=profile, challenge=challenge, level_of_complete__isnull=True)

	# Check that the correct challenge id is sent through to prevent cheating
	if dashboard_extras.get_days_left(user_challenge) <= 0:
		user_challenge.has_completed = True
		user_challenge.save()
	else:
		raise Http404("Malformed URL")

	# set options
	user_results = [(1, user_challenge.challenge.challenge_completion_user_failed),
					(2, user_challenge.challenge.challenge_completion_user_tried),
					(3, user_challenge.challenge.challenge_completion_user_did_good),
					(4, user_challenge.challenge.challenge_completion_user_did_great)]

	if request.method == 'POST':
		if not user_challenge.has_completed:
			raise Http404("Malformed URL")
		form = ChallengeCompleteForm(data=request.POST)
		form.fields['challenge_status'].choices = user_results
		if form.is_valid():
			user_challenge.level_of_complete = form.cleaned_data['challenge_status']
			user_challenge.save()
			points_controller.assign_points(profile, (int(user_challenge.level_of_complete) * int(user_challenge.challenge.challenge_difficulty)) * int(points_controller.get_multiplier()), "challenge", user_challenge.id)
			return HttpResponseRedirect(reverse('challenges:challenge_reward') + "?challenge_id=" + str(id) + "&reward=true")
	else:
		form = ChallengeCompleteForm()
		form.fields['challenge_status'].choices = user_results

	return render(request, 'challenges/complete.html', {'title': ': Complete', 'header_string': get_header_string(profile), 'user_challenge': user_challenge, 'form': form, 'id': id})


# Challenge reward page, shows amount of points the user has gained
def challenge_reward(request):
	# Make sure the passed id hasn't been tampered with
	try:
		id = int(request.GET.get('challenge_id'))
	except ValueError:
		raise Http404("Malformed URL")

	# Get all user and challenge info and check it is correct
	profile = get_object_or_404(UserProfile, user=request.user)
	challenge = get_object_or_404(Challenge, id=id)
	user_challenge = get_object_or_404(ChallengeList, user=profile, challenge=challenge, level_of_complete__isnull=False)
	score = points_controller.get_score_by_type("challenge", user_challenge.id)

	return render(request, 'challenges/reward.html', {'title': ': Reward', 'header_string': get_header_string(profile), 'user_challenge': user_challenge, 'score': score})