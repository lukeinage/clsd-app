from django.contrib import admin

from challenges.models import Challenge, ChallengeList

# registers models for editing in the site backend
admin.site.register(Challenge)
admin.site.register(ChallengeList)