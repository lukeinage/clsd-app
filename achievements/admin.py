from django.contrib import admin
from achievements.models import Achievement, AchievementHistory

# registers models for editing in the site backend
admin.site.register(Achievement)
admin.site.register(AchievementHistory)