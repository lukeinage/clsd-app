from django.db import models
from app_user.models import UserProfile


# A list of individual achievements
class Achievement(models.Model):
	achievement_type = models.CharField(max_length=128)
	achievement_text = models.CharField(max_length=128)

	def __unicode__(self):
		return self.achievement_type


# History of all user achievements
class AchievementHistory(models.Model):
	user = models.ForeignKey(UserProfile)
	achievement = models.ForeignKey(Achievement)
	achievement_date = models.DateTimeField('achievement date')

	def __unicode__(self):
		return self.achievement.achievement_type