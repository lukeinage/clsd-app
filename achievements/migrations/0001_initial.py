# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_user', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Achievement',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('achievement_type', models.CharField(max_length=128)),
                ('achievement_text', models.CharField(max_length=128)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AchievementHistory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('achievement_date', models.DateTimeField(verbose_name=b'achievement date')),
                ('achievement', models.ForeignKey(to='achievements.Achievement')),
                ('user', models.ForeignKey(to='app_user.UserProfile')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
