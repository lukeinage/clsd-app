from django.db import models
import os


def get_media_path(instance, filename):
	file_name, file_extension = os.path.splitext(filename)
	return os.path.join('teams/' + str(instance.pk) + '/img' + file_extension)


# Stores different teams
class Team(models.Model):
	team_name = models.CharField(max_length=128)
	score = models.IntegerField(default=0)
	image = models.ImageField(upload_to=get_media_path, null=True, blank=True)
	signup_date = models.DateTimeField('signup date')

	def __unicode__(self):
		return self.team_name