from django.conf.urls import patterns, url

from teams import views

# routes to the correct view handler
urlpatterns = patterns('',
	url(r'^create/$', views.create_team_view, name='create_team'),
	url(r'^list/$', views.list_team_view, name='list_teams'),
	url(r'^team_detail/$', views.team_detail_view, name='team_detail'),
	url(r'^join/$', views.team_join_view, name='join_team'),
)