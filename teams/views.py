from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, Http404
from django.core.urlresolvers import reverse
from django import forms
from django.utils import timezone
from django.db import IntegrityError
from app_user.models import UserProfile
from django.core.files.images import get_image_dimensions

from teams.models import Team
from app_user.AppUserController import get_header_string


# Form for team creation, checks if uploaded image is too big
class TeamForm(forms.Form):
	team_name = forms.CharField(label='Team Name', max_length=100)
	image = forms.ImageField(label='Upload a team image (Limit 2MB)')

	def clean_image(self):
		picture = self.cleaned_data.get("image")
		if not picture:
			raise forms.ValidationError("There's something wrong with the image")
		else:
			w, h = get_image_dimensions(picture)
			if w > 256:
				raise forms.ValidationError("Width %i pixels wide. Please limit images to 256x256" % w)
			if h > 256:
				raise forms.ValidationError("Height %i pixels wide. Please limit images to 256x256" % h)
			if picture._size > 2*1024*1024:
					raise forms.ValidationError("The image should be less than 2MB")
		return picture


# Team creation handler
def create_team_view(request):
	profile = get_object_or_404(UserProfile, user=request.user)
	if profile.team is None:
		if request.method == 'POST':
			form = TeamForm(request.POST, request.FILES)
			if form.is_valid():
				try:
					t = Team(team_name=form.cleaned_data['team_name'], signup_date=timezone.now())
					t.score += profile.score
					t.save()

					profile.team = t
					profile.save()

					# saving the image after the team has been created
					t.image = form.cleaned_data['image']
					t.save()
					profile.save()
					return HttpResponseRedirect(reverse('dashboard:dash_home'))
				except IntegrityError:
					pass
		else:
			form = TeamForm()
	else:
		raise Http404

	return render(request, 'teams/create_team.html', {'title': ': Create Team', 'header_string': get_header_string(profile), 'form': form})


# Lists all teams by descending order
def list_team_view(request):
	profile = get_object_or_404(UserProfile, user=request.user)
	team_list = Team.objects.order_by('-score')
	return render(request, 'teams/teams.html', {'title': ': Team Leaderboard', 'header_string': get_header_string(profile), 'team_list': team_list})


# Returns full team details
def team_detail_view(request):
	# Make sure the passed id hasn't been tampered with
	try:
		id = int(request.GET.get('team_id'))
	except ValueError:
		raise Http404("Malformed URL")

	profile = get_object_or_404(UserProfile, user=request.user)
	team = get_object_or_404(Team, pk=id)
	team_profiles = UserProfile.objects.filter(team=team).order_by('-score')[:20]

	no_team = False
	if profile.team is None:
		no_team = True

	is_user_team = False
	if profile.team == team:
		is_user_team = True

	return render(request, 'teams/team_detail.html', {'title': ': Team ' + team.team_name, 'header_string': get_header_string(profile), 'team': team, 'team_profiles': team_profiles, 'no_team': no_team, 'team_id': id, 'is_user_team': is_user_team})


# Allows team to be joined
def team_join_view(request):
	# Make sure the passed id hasn't been tampered with
	try:
		id = int(request.GET.get('team_id'))
	except ValueError:
		raise Http404("Malformed URL")

	profile = get_object_or_404(UserProfile, user=request.user)
	team = get_object_or_404(Team, pk=id)

	if profile.team is None:
		profile.team = team
		profile.save()
		team.score += profile.score
		team.save()

	return HttpResponseRedirect(reverse('teams:team_detail') + '?team_id=' + str(id))