# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import teams.models


class Migration(migrations.Migration):

    dependencies = [
        ('teams', '0004_auto_20150211_2013'),
    ]

    operations = [
        migrations.AlterField(
            model_name='team',
            name='image',
            field=models.ImageField(height_field=100, width_field=100, null=True, upload_to=teams.models.get_media_path, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='team',
            name='score',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
    ]
