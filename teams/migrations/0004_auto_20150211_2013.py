# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('teams', '0003_auto_20150211_1958'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='team',
            name='image_url',
        ),
        migrations.AddField(
            model_name='team',
            name='image',
            field=models.ImageField(default=None, upload_to=b'teams'),
            preserve_default=False,
        ),
    ]
