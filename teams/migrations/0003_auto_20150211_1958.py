# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('teams', '0002_team_image_url'),
    ]

    operations = [
        migrations.AlterField(
            model_name='team',
            name='image_url',
            field=models.ImageField(height_field=100, width_field=100, upload_to=b''),
            preserve_default=True,
        ),
    ]
