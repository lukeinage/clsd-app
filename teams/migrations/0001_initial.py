# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('team_name', models.CharField(max_length=128)),
                ('score', models.IntegerField()),
                ('signup_date', models.DateTimeField(verbose_name=b'signup date')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
