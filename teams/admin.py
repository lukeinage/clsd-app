from django.contrib import admin
from teams.models import Team

# registers models for editing in the site backend
admin.site.register(Team)