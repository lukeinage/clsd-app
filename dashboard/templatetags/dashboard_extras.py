from django import template
from django.utils import timezone
import datetime

register = template.Library()


def get_days_left(active_challenge):
	end_date = (active_challenge.start_date + datetime.timedelta(days=active_challenge.challenge.challenge_time_in_days))
	difference = end_date - timezone.now()
	return difference.days + 1

def get_progress_percentage(active_challenge):
	total_days = float(active_challenge.challenge.challenge_time_in_days)
	days_gone = total_days - float(get_days_left(active_challenge))
	return days_gone / total_days * 100

register.filter('get_days_left', get_days_left)
register.filter('get_progress_percentage', get_progress_percentage)