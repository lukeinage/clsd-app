from django.shortcuts import get_object_or_404, render

from app_user.models import UserProfile
from challenges.models import ChallengeList
from teams.models import Team
from app_user.AppUserController import get_dashboard_header_string

# Point of entry, decides which screen should be shown. Dashboard or home splash screen
def dashboard(request):
	if request.user.is_authenticated():
		return setup_dash(request)
	else:
		return setup_splash(request)


# gets user data needed for the dashboard
def setup_dash(request):
	profile = get_object_or_404(UserProfile, user=request.user)
	challenge_list = ChallengeList.objects.filter(user=profile, level_of_complete__isnull=True)
	team_list = Team.objects.order_by('-score')[:3]
	user_team = profile.team
	return render(request, 'dashboard/dashboard.html', {'title': ': Dashboard', 'header_string': get_dashboard_header_string(profile), 'profile': profile, 'user_challenge_list': challenge_list, 'team_list': team_list, 'user_team': user_team})


def setup_splash(request):
	return render(request, 'dashboard/splash.html')

def about(request):
	return render(request, 'dashboard/about.html')