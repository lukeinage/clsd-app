from django.conf.urls import patterns, url
from django.views.generic import TemplateView

from dashboard import views

# routes to the correct view handler
urlpatterns = patterns('',
	url(r'^$', views.dashboard, name='dash_home'),
	url(r'^about/', views.about, name='about'),
)