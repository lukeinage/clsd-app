# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_user', '0003_auto_20150204_1413'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='score',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='team',
            field=models.ForeignKey(blank=True, to='teams.Team', null=True),
            preserve_default=True,
        ),
    ]
