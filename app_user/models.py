from django.db import models
from teams.models import Team
from django.conf import settings


# Stores all user info
class UserProfile(models.Model):
	user = models.ForeignKey(settings.AUTH_USER_MODEL)
	score = models.IntegerField(default=0)
	team = models.ForeignKey(Team, null=True, blank=True)
	twitter_handle = models.CharField(max_length=128)

	def __unicode__(self):
		return self.user.username