def get_header_string(profile):
	header_string = profile.user.username + " -- Score: " + str(profile.score)
	return header_string


def get_dashboard_header_string(profile):
	header_string = "Hey, " + profile.user.first_name + "! -- Score: " + str(profile.score)
	return header_string