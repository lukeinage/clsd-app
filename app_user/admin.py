from django.contrib import admin
from app_user.models import UserProfile

# registers models for editing in the site backend
admin.site.register(UserProfile)