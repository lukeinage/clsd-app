from django.conf.urls import patterns, url
from django.views.generic import TemplateView

from app_user import views

# routes to the correct view handler
urlpatterns = patterns('',
	url(r'^create/$', views.create_user_view, name='create_user'),
	url(r'^login/$', views.login_view, name='log_in'),
	url(r'^logout/$', views.logout_view, name='log_out'),
)