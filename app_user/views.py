from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django import forms
from django.contrib.auth import authenticate, login, logout
from django.db import IntegrityError

from app_user.models import UserProfile

# Form for the main login
class LoginForm(forms.Form):
	username = forms.CharField(label='Username', max_length=100)
	password = forms.CharField(label='Password', max_length=256, widget=forms.PasswordInput)

# Form for signing up users
class UserForm(forms.Form):
	username = forms.CharField(label='Username', max_length=100)
	password = forms.CharField(label='Password', max_length=256, widget=forms.PasswordInput)
	first_name = forms.CharField(label='First Name', max_length=100)
	last_name = forms.CharField(label='Last Name', max_length=100)
	email = forms.EmailField()
	twitter = forms.CharField(label='Twitter', max_length=100)

# Create User Template Handler
def create_user_view(request):
	if request.method == 'POST':
		form = UserForm(request.POST)
		if form.is_valid():
			try:
				user = User.objects.create_user(username=form.cleaned_data['username'], email=form.cleaned_data['email'], password=form.cleaned_data['password'], first_name=form.cleaned_data['first_name'], last_name=form.cleaned_data['last_name'])
				user_profile = UserProfile(user=user, twitter_handle=form.cleaned_data['twitter'], score=0)
				user_profile.save()
				return HttpResponseRedirect(reverse('app_user:log_in'))
			except IntegrityError:
				pass
	else:
		form = UserForm()

	return render(request, 'app_user/create_user.html', {'form': form})

# Login Screen Templater Handler
def login_view(request):
	if request.method == 'POST':
		form = LoginForm(request.POST)
		if form.is_valid():
			user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
			if user is not None:
				if user.is_active:
					login(request, user)
					return HttpResponseRedirect(reverse('dashboard:dash_home'))
	else:
		form = LoginForm()

	return render(request, 'app_user/log_in.html', {'form': form})

# Logs out user and redirects to the home page
def logout_view(request):
	logout(request)
	return HttpResponseRedirect(reverse('dashboard:dash_home'))