from django.db import models

from app_user.models import UserProfile


class Quiz(models.Model):
	title = models.CharField(max_length=64)
	question_total = models.IntegerField()


class UserQuizData(models.Model):
	user = models.ForeignKey(UserProfile)
	quiz = models.ForeignKey(Quiz)
	has_done = models.BooleanField(default=False)
	score = models.IntegerField()


class Question(models.Model):
	quiz = models.ForeignKey(Quiz)
	question_title = models.CharField(max_length=64)


class Answer(models.Model):
	question = models.ForeignKey(Question)
	question_answer = models.CharField(max_length=128)
