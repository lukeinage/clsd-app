from django.contrib import admin
from points.models import Point, PointHistory, PointSetup

# registers models for editing in the site backend
admin.site.register(Point)
admin.site.register(PointHistory)
admin.site.register(PointSetup)