from django.shortcuts import render
from django.utils import timezone

from models import PointHistory, Point


# Adds points to the model
def assign_points(user, point_amount, point_type, point_detail):
	p = Point.objects.get(point_type=point_type)
	if p.exists():
		ph = PointHistory(user=user, point=p, point_amount=point_amount, point_date=timezone.now(), point_detail=point_detail)
		ph.save()
	else:
		raise Exception("Point type doesn't exist")