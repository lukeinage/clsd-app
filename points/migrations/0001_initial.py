# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_user', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Point',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('point_type', models.CharField(max_length=128)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PointHistory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('point_date', models.DateTimeField(verbose_name=b'point date')),
                ('detail', models.CharField(max_length=256)),
                ('point', models.ForeignKey(to='points.Point')),
                ('user', models.ForeignKey(to='app_user.UserProfile')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
