# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('points', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='pointhistory',
            name='detail',
        ),
        migrations.AddField(
            model_name='pointhistory',
            name='detail_id',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
    ]
