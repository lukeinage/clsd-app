# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('points', '0004_pointsettings'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='PointSettings',
            new_name='PointSetup',
        ),
    ]
