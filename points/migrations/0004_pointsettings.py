# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('points', '0003_pointhistory_point_amount'),
    ]

    operations = [
        migrations.CreateModel(
            name='PointSettings',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('point_template_name', models.CharField(max_length=128)),
                ('point_multiplier', models.IntegerField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
