# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('points', '0005_auto_20150208_0006'),
    ]

    operations = [
        migrations.RenameField(
            model_name='pointhistory',
            old_name='detail_id',
            new_name='point_detail_id',
        ),
    ]
