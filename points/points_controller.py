from django.shortcuts import render
from django.utils import timezone

from models import Point, PointHistory, PointSetup


def assign_points(user, point_amount, point_type, point_detail_id):
	p = Point.objects.filter(point_type=point_type)
	if p.exists():
		ph = PointHistory(user=user, point=p[0], point_amount=point_amount, point_date=timezone.now(), point_detail_id=point_detail_id)
		user.score += point_amount
		if user.team:
			user.team.score += point_amount
			user.team.save()
		user.save()
		ph.save()
	else:
		raise Exception("Point type doesn't exist")


def get_multiplier():
	return PointSetup.objects.get(point_template_name="normal").point_multiplier


def get_score_by_type(point_type, point_detail_id):
	p = Point.objects.filter(point_type=point_type)
	if p.exists():
		ph = PointHistory.objects.get(point=p, point_detail_id=int(point_detail_id))
		return ph.point_amount
	else:
		raise Exception("Point type doesn't exist")