from django.db import models

from app_user.models import UserProfile


# A list of individual points that can be collected
class Point(models.Model):
	point_type = models.CharField(max_length=128)

	def __unicode__(self):
		return self.point_type


# History of all points
class PointHistory(models.Model):
	user = models.ForeignKey(UserProfile)
	point = models.ForeignKey(Point)
	point_amount = models.IntegerField()
	point_date = models.DateTimeField('point date')
	point_detail_id = models.IntegerField()

	def __unicode__(self):
		return self.point.point_type


class PointSetup(models.Model):
	point_template_name = models.CharField(max_length=128)
	point_multiplier = models.IntegerField()

	def __unicode__(self):
		return self.point_template_name